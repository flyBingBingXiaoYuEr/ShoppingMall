//
//  TabBarViewController.swift
//  ShoppingMall
//
//  Created by YXY on 2017/12/5.
//  Copyright © 2017年 小鱼儿. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.UI()
    }
    
    func UI() {
        
        let homeNav  = self.addNaviVC(vc: HomeViewController())
        let carNav   = self.addNaviVC(vc: HomeViewController())
        let orderNav = self.addNaviVC(vc: HomeViewController())
        
        
        let mineNav  = self.addNaviVC(vc: HomeViewController())
        self.viewControllers = [homeNav,carNav,orderNav,mineNav]
        let titles:[NSString] = ["首页","购物车","订单","我的"]
        let images:[String] = ["优途","tab_购物车","订单","我的"];
        let selectImgs:[String] = ["优途1","tab_购物车1","订单1","我的1"];
        for item:UITabBarItem in self.tabBar.items! {
            let index = self.tabBar.items!.index(of: item)
            item.title = titles[index!] as String
            item.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.wlYellow,
                                         NSAttributedStringKey.font:k_S_Font],for: UIControlState.selected)
            item.setTitleTextAttributes([NSAttributedStringKey.foregroundColor:UIColor.name,
                                         NSAttributedStringKey.font:k_S_Font], for: UIControlState.normal)
            item.image = UIImage.init(named: images[index!])?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            item.selectedImage = UIImage.init(named: selectImgs[index!])?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        }
        self.setNaviBarStyle()
        self.setTabBarStyle()
    }
    func addNaviVC(vc:UIViewController)->BaseNavigationViewController {
        let naviVC:BaseNavigationViewController = BaseNavigationViewController(rootViewController: vc)
        return naviVC
        
    }
    //设置tabbar的样式
    func setTabBarStyle() {
        self.tabBar.backgroundImage = UIColor.colorToImage(UIColor.white)
        self.tabBar.shadowImage = UIColor.colorToImage(UIColor.line)
    }
    //设置navibar的样式
    func setNaviBarStyle() {
        UINavigationBar.appearance().setBackgroundImage(UIColor.colorToImage(UIColor.white), for: UIBarMetrics.default)
        //        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.wlBlack]
        UINavigationBar.appearance().tintColor = UIColor.wlBlack
        UINavigationBar.appearance().isTranslucent = false
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    
}

