//
//  BaseNavigationViewController.swift
//  ShoppingMall
//
//  Created by YXY on 2017/12/6.
//  Copyright © 2017年 小鱼儿. All rights reserved.
//

import UIKit

class BaseNavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        var fromVC:UIViewController? = nil
        if self.viewControllers.count > 0 {
            fromVC = self.viewControllers.last!
            fromVC?.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
        //viewControllers要加1
        
        if self.viewControllers.count > 0 {
            if self.viewControllers.count == 2 {
                fromVC?.hidesBottomBarWhenPushed = false
            }
        }
        
        //        viewController.hidesBottomBarWhenPushed = true
        //        super.pushViewController(viewController, animated: animated)
    }
}
