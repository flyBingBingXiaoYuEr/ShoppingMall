//
//  BaseData.swift
//  ShoppingMall
//
//  Created by 余潇逸 on 2017/12/7.
//  Copyright © 2017年 小鱼儿. All rights reserved.
//

import UIKit
import ObjectMapper

class BaseData: NSObject, Mappable {

    var code:Int?
    var message:String?
    
    override init(){
        super.init()
    }
    
    required init(map: Map) {
        super.init()
        code     <- map["result"]
        message     <- map["message"]
    }
    
    func mapping(map: Map) {
        
    }
    
    func isSuc() -> Bool {
        return code == 1
    }
}
