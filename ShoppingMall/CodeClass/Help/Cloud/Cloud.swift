//
//  Cloud.swift
//  ShoppingMall
//
//  Created by 余潇逸 on 2017/12/5.
//  Copyright © 2017年 小鱼儿. All rights reserved.
//

import Foundation
import Alamofire
//import SwiftyJSON

class Cloud {
    
    static let shareCloud = Cloud()
    
    static let mainApiString = "http://api.rana360.com"
    
    let header : HTTPHeaders = [
        "Content-Type":"application/json",
        "Accept":"application/json"
    ]

    var requestManager: SessionManager = {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 15
        return SessionManager(configuration: config)
        //再试一次 能否成功
        //再试最后一次
    }()
 
    func createHeaderDic() -> [String : Any] {
        let infoDictionary = Bundle.main.infoDictionary
        let headerDic: [String : Any] = [
            "AppVersion": infoDictionary? ["CFBundleShortVersionString"] ?? "1.0", // 客户端版本号
            "Platform": 1, // 系统类型（0安卓  1苹果
        ]
        return headerDic
    }
    
    func createParameters(params : [String : Any]?) -> [String : Any] {
        var parameters: [String : Any] = [
            "Header": createHeaderDic(),
            "UserAc":UserDefaults.standard.object(forKey: "UserAc") ?? "",
            "ExtParams": "",
            ]
        if params != nil {
            parameters["Body"] = params
        }
        return parameters
    }
    
}
