//
//  TTCheck.swift
//  ShoppingMall
//
//  Created by 余潇逸 on 2017/12/7.
//  Copyright © 2017年 小鱼儿. All rights reserved.
//

import Foundation
import UIKit

func printLog<T>(_ message : T){
    if Constants.isDebug {
        print(message)
        print("\n")
    }
}
