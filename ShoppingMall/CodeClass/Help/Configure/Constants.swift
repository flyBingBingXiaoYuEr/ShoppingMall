//
//  Constants.swift
//  ShoppingMall
//
//  Created by 余潇逸 on 2017/12/7.
//  Copyright © 2017年 小鱼儿. All rights reserved.
//

import Foundation
public final class ConstantInstance {
    
}

struct Constants {
    fileprivate enum AppConfigType {
        case debug
        case relese
        case releaseTest
    }
    
    fileprivate static var currentConfig: AppConfigType {
//        #if DEBUG = 1
            return .debug
//        #elseif RELEASE_TEST = 1
//            return .releaseTest
//        #else
//            return .relese
//        #endif
    }
    
    static var serverURL: String {
        switch currentConfig {
        case .debug:
            //李勇
            //            return "http://192.168.11.185:8080/"
            //国宴
            return "http://192.168.11.90:8080/"
            //            return  "http://foavi2.asuscomm.com:5353/"
            
        //            return "http://www.uotooo.com/"
        case .releaseTest:
            //             return  "http://foavi2.asuscomm.com:7373/"
            //            return  "http://foavi2.asuscomm.com:5353/"
            return "http://www.uotooo.com/"
        default:
            return "http://www.uotooo.com/"
            //            return  "http://foavi2.asuscomm.com:5353/"
        }
    }
    
    static var isDebug: Bool {
        switch currentConfig {
        case .debug:
            return true
        case .releaseTest:
            return true
        default:
            return false
        }
    }
    
    /* 友盟 */
    static let umAppKey = "584677593eae25075e000965"
    
    /* 微信 */
    static let wechatAppKey = "wx0859438800a3f7fe"
    static let wechatSecret = "db426a9829e4b49a0dcac7b4162da6b6"
    
    /* QQ */
    static let qqAppKey = "1105025352"
    //    "CryWkHjVwnY0lLGs"
    
    /* 激光推送 */
    static let jpushAppkey = "45569763caba548067142207"
    static let jpushChannel = "Publish channel"
    
    /* 违章查询key */
    static let violateKey = "72045fc126fcbc179611332081ce2b01"
}
