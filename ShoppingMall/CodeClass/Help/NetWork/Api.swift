//
//  Api.swift
//  ShoppingMall
//
//  Created by 余潇逸 on 2017/12/6.
//  Copyright © 2017年 小鱼儿. All rights reserved.
//

import Foundation
import Moya

enum Api: String {
    // 登录
    case login = "app/api/login/login_enter.do"
    case logout = "app/api/login/logout.do"
    case userCenter = "app/api/userinfo/user_mycenter.do"
    case bindJpush = "member/Profile/update"
    
    var url: String {
        return Constants.serverURL + self.rawValue
    }
    
    var fullUrl: String {
        return self.rawValue
    }
}
enum MoyaApi {
    // 登录
    case login
    case logout
    case userCenter
    case bindJpush
}
extension MoyaApi: TargetType {
    var task: Task {
        return .requestPlain
    }
    
    var headers: [String : String]? {
        return ["" : ""]
    }
    
    var baseURL: URL {
        return URL(string: Constants.serverURL)!
    }
    
    var path: String {
        switch self {
        case .login:
            return "app/api/login/login_enter.do"
        case .logout:
            return "app/api/login/logout.do"
        case .userCenter:
            return "app/api/userinfo/user_mycenter.do"
        case .bindJpush:
            return ""
        }
    }

    var method: Moya.Method {
        return .get
    }


    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    var sampleData: Data {
        return "Create post successfully".data(using: String.Encoding.utf8)!
    }

    var validate: Bool {
        return false
    }
}
