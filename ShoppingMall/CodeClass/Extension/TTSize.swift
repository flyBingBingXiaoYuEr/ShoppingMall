//
//  TTSize.swift
//  TT_Swift
//
//  Created by Lenny on 16/5/12.
//  Copyright © 2016年 Lenny. All rights reserved.
//

import Foundation
import UIKit

//尺寸

let k_screen_w:CGFloat = UIScreen.main.bounds.size.width
let k_screen_h:CGFloat = UIScreen.main.bounds.size.height
let k_navi_h:CGFloat = 64.0
let k_race:CGFloat = k_screen_w / 375.0

//字体
let k_M_Font = UIFont.systemFont(ofSize: 15 * k_race)
let k_Ms_Font = UIFont.systemFont(ofSize:13 * k_race)
let k_S_Font = UIFont.systemFont(ofSize:12 * k_race)
let k_Ss_Font = UIFont.systemFont(ofSize:10 * k_race)
let k_B_Font = UIFont.systemFont(ofSize:17 * k_race)
//加粗
let k_M_B_Font = UIFont.boldSystemFont(ofSize: 15 * k_race)
let k_S_B_Font = UIFont.boldSystemFont(ofSize: 12 * k_race)
let k_B_B_Font = UIFont.boldSystemFont(ofSize: 17 * k_race)
let k_BB_B_Font = UIFont.boldSystemFont(ofSize: 23 * k_race)

let k_BB_BBB_Font = UIFont.boldSystemFont(ofSize: 40 * k_race)
let k_win = UIApplication.shared.windows.first!
