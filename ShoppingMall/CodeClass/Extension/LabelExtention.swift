//
//  UILabel_Ex.swift
//  JZB
//
//  Created by Lenny on 16/7/19.
//  Copyright © 2016年 Lenny. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    // 快速创建label
    convenience init(title: String = "", titleColor: UIColor, font: UIFont) {
        self.init()
        self.text = title
        self.textColor = titleColor
        self.font = font
    }
}
